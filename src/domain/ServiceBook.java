/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author Dragon
 */
public class ServiceBook implements Serializable{
    
    private Long serviceBookID;
    private String clientFirstName;
    private String clientLastName;
    private String vehicleDescription;
    private LocalDate initialDate;
    private boolean active;

    public ServiceBook() {
    }

    public ServiceBook(Long serviceBookID, String clientFirstName, String clientLastName, String vehicleDescription, LocalDate initialDate, boolean active) {
        this.serviceBookID = serviceBookID;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.vehicleDescription = vehicleDescription;
        this.initialDate = initialDate;
        this.active = active;
    }

    public Long getServiceBookID() {
        return serviceBookID;
    }

    public void setServiceBookID(Long serviceBookID) {
        this.serviceBookID = serviceBookID;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public LocalDate getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(LocalDate initialDate) {
        this.initialDate = initialDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
